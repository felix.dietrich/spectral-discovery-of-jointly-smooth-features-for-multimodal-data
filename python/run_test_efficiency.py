import numpy as np
import matplotlib.pyplot as plt

import scipy
import scipy.sparse.linalg

from time import time

# install this with "pip install datafold"
import datafold.pcfold as pfold
import pandas as pd

from large_eigensystems import *
from test_efficiency import *

import warnings
from scipy.sparse import SparseEfficiencyWarning
warnings.simplefilter('ignore',SparseEfficiencyWarning)


kernel_evecs = 100
cknn_k_neighbor = 25
evec_tol = 1e-4
kernel_tol = 1e-11

example = "torus" # "square"

if example == "square":
    def sensor1(_x,_y):
        return np.column_stack([
            _x,
            _y
        ])
    def sensor2(_x,_z):
        return np.column_stack([
            (1+_z+_x/4)/3 * np.cos(3*np.pi*_z),
            (1+_z+_x/4)/3 * np.sin(3*np.pi*_z)
        ])
    get_peff = None
elif example == "torus":
    def sensor1(_z,_eps):
        _z = _z+0.5
        _eps = _eps+0.5
        return np.column_stack([
            (3/2 * _eps + _z /3 + 2/3) *np.cos(4*np.pi*_eps),
            (3/2 * _eps + _z /3 + 2/3) *np.sin(4*np.pi*_eps)
        ])
    def sensor2(_z,_nu):
        _z = _z+0.5
        _nu = _nu+0.5
        return np.column_stack([
            (1+1/3*np.cos(2*np.pi*_z))*np.cos(2*np.pi*_nu),
            (1+1/3*np.cos(2*np.pi*_z))*np.sin(2*np.pi*_nu),
            1/3*np.sin(2*np.pi*_z)
        ])
    def get_peff(x,y,z): return x[:,0];# = lambda x,y,z: x


def run_test(run_test=True):
    n_pts_all = [50000] # np.arange(1,61,5).astype(np.int) * 5000
    n_eigenvectors_all = np.arange(1,11,1).astype(np.int) * 100 # kernel_evecs
    print(n_pts_all)
    
    if run_test:
        randint = np.random.randint(100000000)
        random_seeds = np.arange(0,10)

        time_data_all = pd.DataFrame()

        _idx = 0
        for n_pts in n_pts_all:
            for n_eigenvectors in n_eigenvectors_all:
                for random_seed in random_seeds:
                    t0 = time()
                    _te = TestEfficiency(sensor1, sensor2, n_pts=n_pts,cknn_k_neighbor=cknn_k_neighbor,
        	                             random_seed=random_seed, verbose=False, n_kernel_evecs=n_eigenvectors, kernel_tol=kernel_tol, 
        	                             evec_tol=evec_tol,
        	                             get_peff=get_peff, kernel_type="cknn")
                    Cevals, Cevecs = _te.compute_common_system()
                    time_data_all = time_data_all.append(pd.DataFrame.from_records([_te.time_data.values()], columns=_te.time_data.keys()))
                    _idx += 1
                    _k1_sparsity = _te.time_data["mat_neighbors_per_row_k1"]
                    _k2_sparsity = _te.time_data["mat_neighbors_per_row_k1"]
                    print(f"\rCompleted {_idx} of {len(n_eigenvectors_all)*len(n_pts_all)*len(random_seeds)} at {n_pts} points, {n_eigenvectors} eigenvectors, took {time()-t0} seconds total. K1,K2 neighbors per row: {_k1_sparsity}, {_k2_sparsity}")

                    time_data_all.to_csv('time_data-'+str(randint)+'.csv', index=False)
        
if __name__ == "__main__":
    run_test()
