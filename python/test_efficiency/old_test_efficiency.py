import numpy as np
import matplotlib.pyplot as plt

import scipy
import scipy.sparse.linalg

from time import time

# install this with "pip install datafold"
import datafold.pcfold as pfold

import pandas as pd

from large_eigensystems import *


class TestEfficiency():
    def __init__(self, sensor1, sensor2, verbose=True, n_pts = 10000, random_seed = 1, n_kernel_evecs=500, n_common_evecs=10, cutoff_result_scaling=1, kernel_tol=1e-8, evec_tol=1e-6, cknn_k_neighbor=50, cknn_delta=1, compute_sparse=True, get_peff=None, kernel_type="gaussian"):
        # parameters
        self.n_pts = n_pts
        self.random_seed = random_seed

        self.verbose = verbose
        self.n_kernel_normalizations = 3
        self.evec_tol = evec_tol
        self.kernel_tol = kernel_tol
        self.cknn_k_neighbor = cknn_k_neighbor
        self.cknn_delta = cknn_delta

        self.n_kernel_evecs = n_kernel_evecs
        self.n_common_evecs = n_common_evecs

        self.compute_sparse = compute_sparse

        self.sensor1 = sensor1
        self.sensor2 = sensor2
        self.get_peff = get_peff
        
        self.cutoff_result_scaling = cutoff_result_scaling
        
        self.kernel_type = kernel_type
        
        self.time_data = dict()
        self.time_data["n_pts"] = self.n_pts
        self.time_data["n_kernel_evecs"] = self.n_kernel_evecs
        self.time_data["n_common_evecs"] = self.n_common_evecs
        self.time_data["random_seed"] = self.random_seed
        
    def __log(self, message):
        if self.verbose:
            print(message)

    def kernelmatrix(self, data, eps = None):
        
        if self.kernel_type == "gaussian":
            # estimate a good cutoff
            _pcm = pfold.PCManifold(data, dist_kwargs=dict(backend="scipy.kdtree"))
            _pcm.optimize_parameters(result_scaling=self.cutoff_result_scaling)
            cut_off = _pcm.cut_off
            
            res = scipy.sparse.csr_matrix(_pcm.compute_kernel_matrix())
        else:
            # compute the CKNN kernel
            cut_off = 1e-16
            _k = pfold.kernels.ContinuousNNKernel(k_neighbor=self.cknn_k_neighbor, delta=self.cknn_delta)
            _pcm = pfold.PCManifold(data, kernel=_k, dist_kwargs=dict(backend="scipy.kdtree",cut_off=cut_off, kmin=self.cknn_k_neighbor))
            res = scipy.sparse.csr_matrix(_pcm.compute_kernel_matrix()[0])

        res = 1/2 * scipy.sparse.csr_matrix(res + res.T)
        res = scipy.sparse.diags(np.sqrt(np.array(1/res.sum(axis=0))).ravel(), 0) @ res @ scipy.sparse.diags(np.sqrt(np.array(1/res.sum(axis=0))).ravel(), 0)
        res = scipy.sparse.diags(np.array(1/res.sum(axis=0)).ravel(), 0) @ res
        res.eliminate_zeros()

        return res

    def setup_example(self):
        t0 = time()
        
        rng = np.random.default_rng(self.random_seed)

        xyz = rng.uniform(low=-.5,high=.5,size=(self.n_pts,3))
        x,y,z = xyz[:,0].reshape(-1,1), xyz[:,1].reshape(-1,1), xyz[:,2].reshape(-1,1)

        s1 = self.sensor1(x, y)
        
        if self.get_peff is None:
            peff = s1[:,0]+1*s1[:,1]**2
        else:
            peff = self.get_peff(x,y,z)

        s2 = self.sensor2(peff, z[:,0])
        
        self.time_data["time_example"] = time()-t0
        return s1, s2, peff

    def compute_kernels(self):
        s1, s2, peff = self.setup_example()
        
        t0 = time()
        k1 = self.kernelmatrix(s1)
        self.time_data["time_k1"] = time()-t0
        self.time_data["mat_sparsity_k1"] = k1.count_nonzero() / np.prod(k1.shape) * 100
        self.time_data["mat_neighbors_per_row_k1"] = int(k1.count_nonzero() / k1.shape[0])
        
        self.__log(f"Time for kernel 1: {self.time_data['time_k1']}")
        self.__log(f"Sparsity of kernel 1: {self.time_data['mat_sparsity_k1']} percent")

        t0 = time()
        k2 = self.kernelmatrix(s2)
        self.time_data["time_k2"] = time()-t0
        self.time_data["mat_sparsity_k2"] = k2.count_nonzero() / np.prod(k2.shape) * 100
        self.time_data["mat_neighbors_per_row_k2"] = int(k2.count_nonzero() / k2.shape[0])
        
        self.__log(f"Time for kernel 2: {self.time_data['time_k2']}")
        self.__log(f"Sparsity of kernel 2: {self.time_data['mat_sparsity_k2']} percent")

        return k1, k2

    def compute_common_system(self):
        k1, k2 = self.compute_kernels()
        
        CE = CommonEigensystemMatrix(k1, k2, kernel_tol=self.kernel_tol, kernel_evecs=self.n_kernel_evecs, verbose=self.verbose, evec_tol=self.evec_tol)

        self.time_data["time_eig_k1"] = CE.time_data["time_eig_k1"]
        self.time_data["time_eig_k2"] = CE.time_data["time_eig_k2"]
        
        if self.compute_sparse:
            t0=time()
            
            more_than_two = True
            
            if more_than_two:
                Cevecs_sparse,Cevals_sparse,_ = scipy.sparse.linalg.svds(CE.rows, k=self.n_common_evecs, which='LM', tol=self.evec_tol)
                Cevals,Cevecs = CE._sort_eigensystem(Cevals_sparse, Cevecs_sparse)
            else: # TODO: make this sparse!
                _Q,_Gamma,_Rt = scipy.sparse.linalg.svds(CE.svd_matrix, k=self.n_common_evecs, which='LM', tol=self.evec_tol)
                _center = np.row_stack([np.column_stack([_Q, _Q]), np.column_stack([_Rt.T,_Rt.T])])
                _right = np.diag(np.power(np.concatenate([1+_Gamma, 1-_Gamma]), -1/2))
                Cevecs = 1/np.sqrt(2) * CE.rows @ _center @ _right
                Cevals = _Gamma
                
            self.time_data["time_common"] = time()-t0
            
            self.__log(f"Time for sparse SVD {time()-t0}")
            
        if not (self.compute_sparse):
            t0=time()
            Cevecs_dense,Cevals_dense,_ = scipy.linalg.svd(CE.rows)
            self.time_data["time_common"] = time()-t0
            
            self.__log(f"Time for dense SVD {time()-t0}")

            Cevals,Cevecs = CE._sort_eigensystem(Cevals_dense, Cevecs_dense)

        return Cevals, Cevecs