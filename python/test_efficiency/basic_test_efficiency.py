import numpy as np
import scipy.sparse
import scipy.sparse.linalg
from datafold.pcfold import GaussianKernel, PCManifold, PCManifoldKernel

from jointly_smooth_functions import normalize_csr_matrix, sort_eigensystem
from large_eigensystems import CommonEigensystemMatrix
from test_efficiency.abc_test_efficiency import TestEfficiency


class BasicTestEfficiency(TestEfficiency):
    def __init__(
        self,
        sensor1,
        sensor2,
        n_points,
        n_kernel_eigenvectors,
        n_jointly_smooth_functions,
        kernel,
        kernel_eigenvalue_cut_off,
        eigenvector_tolerance,
        get_peff,
        random_seed,
        verbose,
        result_scaling=1.0,
        compute_sparse=True,
        dist_kwargs=None,
    ):
        super().__init__(
            sensor1=sensor1,
            sensor2=sensor2,
            n_points=n_points,
            n_kernel_eigenvectors=n_kernel_eigenvectors,
            n_jointly_smooth_functions=n_jointly_smooth_functions,
            kernel=kernel,
            kernel_eigenvalue_cut_off=kernel_eigenvalue_cut_off,
            eigenvector_tolerance=eigenvector_tolerance,
            get_peff=get_peff,
            random_seed=random_seed,
            verbose=verbose,
            dist_kwargs=dist_kwargs,
        )

        self.result_scaling = result_scaling
        self.compute_sparse = compute_sparse

    def compute_common_system(self):
        k1, k2 = self._compute_kernels()

        common_eigensystem = self._time_method(
            'time_common_eigensystem_matrix',
            lambda: CommonEigensystemMatrix(
                _k1=k1,
                _k2=k2,
                verbose=self.verbose,
                kernel_evecs=self.n_kernel_eigenvectors,
                kernel_tol=self.kernel_eigenvalue_cut_off,
                evec_tol=self.eigenvector_tolerance
            )
        )

        if self.compute_sparse:
            left_singular_vectors, eigenvalues = self._time_method(
                'time_svd_calculation',
                lambda: self._compute_common_system_sparse(common_eigensystem)
            )
        else:
            left_singular_vectors, eigenvalues = self._time_method(
                'time_svd_calculation',
                lambda: self._compute_common_system(common_eigensystem)
            )

        self.time_data['time_jsf_calculation'] = self.time_data['time_common_eigensystem_matrix'] + \
                                                 self.time_data['time_svd_calculation'] + \
                                                 self.time_data['time_kernel1'] + \
                                                 self.time_data['time_kernel2']

        eigenvalues, left_singular_vectors = sort_eigensystem(eigenvalues, left_singular_vectors)

        sparse_or_dense = 'sparse' if self.compute_sparse else 'dense'
        time_elapsed = self.time_data['time_jsf_calculation']
        self._log(f'Time for {sparse_or_dense} SVD: {time_elapsed}')

        return eigenvalues, left_singular_vectors

    def _compute_kernels(self):
        s1, s2, peff = self.setup_example()
        k1 = self._time_method('time_kernel1', lambda: self._compute_kernel_matrix(s1))
        k2 = self._time_method('time_kernel2', lambda: self._compute_kernel_matrix(s2))

        return k1, k2

    def _compute_kernel_matrix(self, data):
        if isinstance(self.kernel, GaussianKernel):
            pcm = PCManifold(data, dist_kwargs=self.dist_kwargs)
            pcm.optimize_parameters(result_scaling=self.result_scaling)
        else:
            pcm = PCManifold(data, kernel=self.kernel, dist_kwargs=self.dist_kwargs)

        kernel_output = pcm.compute_kernel_matrix()
        kernel_matrix, _, _ = PCManifoldKernel.read_kernel_output(kernel_output)
        sparse_kernel_matrix = scipy.sparse.csr_matrix(kernel_matrix, dtype=np.float64)
        sparse_kernel_matrix = normalize_csr_matrix(sparse_kernel_matrix)

        return sparse_kernel_matrix

    def _compute_common_system_sparse(self, common_eigensystem):
        more_than_two = True

        if more_than_two:
            left_singular_vectors, eigenvalues, _ = scipy.sparse.linalg.svds(
                common_eigensystem.rows,
                k=self.n_jointly_smooth_functions,
                which='LM',
                tol=self.eigenvector_tolerance,
            )
        else:
            Q, eigenvalues, R_t = scipy.sparse.linalg.svds(
                common_eigensystem.svd_matrix,
                k=self.n_jointly_smooth_functions,
                which='LM',
                tol=self.eigenvector_tolerance,
            )
            center = np.row_stack([np.column_stack([Q, Q]), np.column_stack([R_t.T, -R_t.T])])
            right = np.diag(np.power(np.concatenate([1 + eigenvalues, 1 - eigenvalues]), -1 / 2))
            left_singular_vectors = 1 / np.sqrt(2) * common_eigensystem.rows @ center @ right

        return left_singular_vectors, eigenvalues

    def _compute_common_system(self, common_eigensystem):
        left_singular_vectors, eigenvalues, _ = scipy.linalg.svd(common_eigensystem.rows)
        return left_singular_vectors, eigenvalues
